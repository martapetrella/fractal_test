'use strict';

/* Create a new Fractal instance and export it for use elsewhere if required */
const fractal = module.exports = require('@frctl/fractal').create();

/* Set the title of the project */
fractal.set('project.title', 'Libreria di Marta');

/* Tell Fractal where the components will live */
fractal.components.set('path', __dirname + '/src/components');

/* Tell Fractal where the documentation pages will live */
fractal.docs.set('path', __dirname + '/src/docs');

/* Specify a directory of static assets */
fractal.web.set('static.path', __dirname + '/src/public');

/* Set the static HTML build destination */
fractal.web.set('builder.dest', __dirname + '/build');

/* default status*/
fractal.components.set('default.status', 'wip');

// default preview
fractal.components.set('default.preview', '@template-default');


// risorse (non funziona ma lo tengo come appunto)
// fractal.components.set('resources', {
//     scss: {
//         label: 'SCSS',
//         match: ['**/*.scss']
//     },
//     css: {
//         label: 'CSS',
//         match: ['**/*.css']
//     },
//     other: {
//         label: 'Other Assets',
//         match: ['**/*', '!**/*.scss', '!**.css']
//     }
// });
