module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      scripts: {
        files: ['src/assets/**/*.*'],
        tasks: ['sass'],
        options: {
          spawn: false,
        },
      },
    },
    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          'src/public/css/style.css': 'src/assets/scss/style.scss', // 'destination': 'source'
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.registerTask('default', ['watch']);
};
