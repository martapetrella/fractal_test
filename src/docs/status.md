### Components

#### Prototype
  * **description:** Do not implement.,
  * **color:** #FF3333

#### Wip:
  * **description**  Work in progress. Implement with caution. ,
  * **color**  #FF9233

#### Ready:
  * **description**  Ready to implement. ,
  * **color**  #29CC29


### Documentation

#### Draft:
  * **description** Work in progress.,
  * **color** #FF3333

#### Ready:
  * **description** Ready for referencing.,
  * **color** #29CC29
